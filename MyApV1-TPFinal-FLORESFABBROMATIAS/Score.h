#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class Score
{
private:
	int points = 0;
	Font *typo;
	Text *scoreText;
public:
	Score();
	void DrawScore(RenderWindow*);
	void UpdateText();
	void RestartScore();
	int GetScore() { return points; };
	void AddPoints(int);
	void SubstractPoints(int);
	~Score();
};

