#include "Target.h"
#include "Globals.h"   

Target::Target(Vector2f spawnPoint, Vector2f force)
{
	position = spawnPoint;
	
	acceleration = gravity;
	speed = force;
	vInit = speed.y;

	type = rand()% 2; //type = 0 normal | 1 = bad

	targetTexture = new Texture;
	targetSprite = new Sprite;

	if (type == 0)
		targetTexture->loadFromFile("Sprites/vinil.png");
	else
		targetTexture->loadFromFile("Sprites/vinilRed.png");

	targetSprite->setTexture(*targetTexture);
	targetSprite->setScale(0.4f, 0.4f);
	targetSprite->setPosition(position);

	bounds = targetSprite->getGlobalBounds();

	bufferFly = new SoundBuffer;
	bufferFly->loadFromFile("Sounds/flying.ogg");
	bufferHit = new SoundBuffer;
	bufferHit->loadFromFile("Sounds/smash.ogg");
	
	sound = new Sound;
}

void Target::Draw(RenderWindow *window)
{
	if (drawme)	window->draw(*targetSprite);
}

void Target::Update(float dTime)
{
	float t;
	t = ((float)dTime) / CLOCKS_PER_SEC;
	
	distance.x = position.x + speed.x * t;
	distance.y = vInit * t + 0.5*acceleration*(pow(t,2));

	position.x = distance.x;
	position.y = position.y + distance.y;
	
	targetSprite->setPosition(position);
}

int Target::CheckHit(Vector2f *impact)
{
	Vector2f aux = *impact;
	if (drawme)
	{
		bounds = targetSprite->getGlobalBounds();

		if (bounds.contains(aux))
		{
			Hitted();
			if (type == 0) return 1; else return 2;
		}
		else return 0;
	}
	else return 0;
}

int Target::IfOutScreen()
{
	if (isActive)
	{ 
		if (type == 0)
		{
			if (position.x > 799 || position.y > 599)
			{
				//isActive = false;
				if (drawme)
				{
					return 2;
				}
				else
					return 1;
			} 
			else 
				return 0;
		}
		else if (position.x > 799 || position.y > 599)
		{
			return 1;
		}
	}
	return 0;
}

void Target::Hitted()
{
	drawme = false;
	sound->setVolume(20);
	sound->setBuffer(*bufferHit);
	sound->play();
}

Target::~Target()
{
	delete(targetSprite);
	delete(bufferFly);
	delete(bufferHit);
	delete(sound);
	delete(targetTexture);
}
