#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

using namespace sf;

class Player
{
private:
	Vector2i mousePosition;
	Texture *dotTexture, *gunTexture;
	Sprite *dotSprite, *gunSprite;
	SoundBuffer *soundBuffer;
	Sound *shootEffect;
	Font *font;
	Text *livesText;
	int lives = 3;

	void PlaySound();
public:
	Player();
	void Render(RenderWindow*);
	void ResetPlayer();
	void Shoot();
	void SubstractLife() {
		lives--; 
		livesText->setString("Vidas: " + std::to_string(lives));
	};
	bool GetLives() { return lives; };
	Vector2i GetCurrentPosition();
	void Move(RenderWindow*);
};

