#include "Game.h"
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

Game::Game()
{
	window = new RenderWindow(VideoMode(800,600,32), "MRUV");
	window->setFramerateLimit(60);
	window->setMouseCursorVisible(false);
	tInitial = clock();
	
	eventManager = new Event;
	scoreSystem = new Score();
	player = new Player();
	
	skyTexture = new Texture;
	skyTexture->loadFromFile("Sprites/toonSky.png");
	skySprite = new Sprite;
	skySprite->setTexture(*skyTexture);

	groundTexture = new Texture();
	groundTexture->loadFromFile("Sprites/ground.png");
	groundSprite = new Sprite();
	groundSprite->setTexture(*groundTexture);
	groundSprite->setPosition(0, 550);

	isPlaying = true;

	deltaTime = 0;
	Vector2f coords = Vector2f(-5, 200);
	target = new Target(coords, initialForce);
}

void Game::Update()
{
	while (window->isOpen())
	{
		while (window->pollEvent(*eventManager))
		{
				switch (eventManager->type)
				{
				case Event::Closed:
					window->close();
					break;
				case Event::MouseButtonReleased:
					if (eventManager->key.code == Mouse::Left)
					{
						player->Shoot();

						convertedPosition = &window->mapPixelToCoords(player->GetCurrentPosition());
						int aux = 0;
						if (target != nullptr)
						{
							aux = target->CheckHit(convertedPosition);
						}
							

						if (aux == 1)
						{
							scoreSystem->AddPoints(10);
						}

						if (aux == 2)
						{
							scoreSystem->SubstractPoints(15);
						}

						if (isPlaying == false)
						{
							int reply = gameOverScreen->Response(convertedPosition);

							if (reply == 1)
							{
								//reinicia la partida
								RestartGame();

							}
							else if (reply == 2)
							{
								//se termina el juego
								exit(EXIT_FAILURE);
								//gameOver = true;
							}
							
						}

					}
					break;
				default:
					break;
				}
		}
		Physics();
		Draw();
	}
}

void Game::Physics()
{
	player->Move(window);
	if (target != nullptr) 
	{
		if (isPlaying)
		{
			deltaTime = clock() - tInitial;
			target->Update(deltaTime);
			if (target->IfOutScreen() == 2)
			{
				tInitial = clock();
				ReActivateTarget();

				player->SubstractLife();
				if (player->GetLives() == 0)
				{
					gameOverScreen = new GameOverScreen({265,200}, scoreSystem->GetScore());
					isPlaying = false;
					gameOverScreen->GameOverTime();
				}
			}
			else if (target->IfOutScreen() == 1)
			{
				tInitial = clock();
				ReActivateTarget();
			}
		}
	}
}

void Game::Draw()
{
	window->clear();

	window->draw(*skySprite);
	window->draw(*groundSprite);
	//Target
	if (target != nullptr) 
	{
		target->Draw(window);
	}

	if (isPlaying == false)
	{
		gameOverScreen->Render(window);
	}
	
	scoreSystem->DrawScore(window);
	player->Render(window);

	window->display();
}

void Game::ReActivateTarget()
{
	Vector2f startPos;
	startPos.x = rand() % -10, -5;
	startPos.y = rand() % 100, 350;
	initialForce.x = rand() % 15, 35;
	initialForce.y = rand() % -10, -30;
	delete(target);
	target = new Target(startPos, initialForce);
}

void Game::RestartGame()
{
	scoreSystem->RestartScore();
	isPlaying = true;
	tInitial = clock();
	ReActivateTarget();
	player->ResetPlayer();
}
