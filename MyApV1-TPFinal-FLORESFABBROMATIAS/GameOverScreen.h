#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

using namespace sf;

class GameOverScreen
{
private:
	Font *font;
	Text *gameOverText, *questionText;
	Sprite *sprYes, *sprNo;
	Texture *texYes, *texNo;
	bool isShowing = false;
	int finalScore;
	Vector2f position;
	FloatRect yesRect, noRect;

public:
	GameOverScreen(Vector2f startPosition, int score);
	void Render(RenderWindow*);
	bool GetIsShowing() { return isShowing; };
	int Response(Vector2f*);
	void GameOverTime();
	~GameOverScreen();
};

