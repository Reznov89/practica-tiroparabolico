#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>

#include "Score.h"
#include "Player.h"
#include "GameOverScreen.h"
#include "Target.h"
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <cstdlib>

using namespace sf;

class Game
{
private:
	RenderWindow *window;
	Event *eventManager;
	Score *scoreSystem;
	Player *player;
	Texture *skyTexture, *groundTexture;
	Sprite *skySprite, *groundSprite;
	GameOverScreen *gameOverScreen;
	clock_t tInitial;
	Vector2f initialForce{ 5, -5 };
	Target *target;
	bool isPlaying = false;
	float deltaTime;
	Vector2f *convertedPosition;

	void ReActivateTarget();
	void Physics();
	void Draw();
	void RestartGame();
public:
	Game();
	void Update();
};

