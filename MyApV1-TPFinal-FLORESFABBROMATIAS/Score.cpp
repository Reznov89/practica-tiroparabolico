#include "Score.h"

Score::Score()
{
	typo = new Font;
	typo->loadFromFile("Fonts/tahomabd.ttf");

	scoreText = new Text;
	scoreText->setFont(*typo);
	UpdateText();

	scoreText->setFillColor(Color::Black);
	scoreText->setPosition(350, 5);
	scoreText->setCharacterSize(14);
}

void Score::DrawScore(RenderWindow *window)
{
	window->draw(*scoreText);
}

void Score::UpdateText()
{
	scoreText->setString("Points: " + std::to_string(points));
}

void Score::RestartScore()
{
	points = 0;
	UpdateText();
}

void Score::AddPoints(int amount)
{
	points += amount;
	UpdateText();
}

void Score::SubstractPoints(int amount)
{
	points -= amount;
	UpdateText();
}

Score::~Score()
{
}
