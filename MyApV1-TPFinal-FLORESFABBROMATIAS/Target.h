#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace sf;

class Target
{
private:
	Texture *targetTexture;
	Sprite *targetSprite;
	SoundBuffer *bufferFly, *bufferHit;
	Sound *sound;
	Vector2f position;
	
	//yeah bitches, science!
	float acceleration;
	float vInit = 0;
	float vFin = 0;
	Vector2f speed;
	Vector2f distance=Vector2f(0,0);
	float movementTime = 0.1f;
	
	FloatRect bounds;
	int type;
	bool drawme = true, isActive = true;

	void Hitted();
public:
	Target(Vector2f, Vector2f);
	void Draw(RenderWindow*);
	void Update(float dTime);
	int CheckHit(Vector2f*);
	int IfOutScreen();
	void ActivateTarget() { isActive = true; };
	void DeactivateTarget() { isActive = false; };
	~Target();
};

