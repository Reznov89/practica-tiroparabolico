#include "Player.h"

Player::Player()
{
	dotTexture = new Texture;
	dotTexture->loadFromFile("Sprites/dot.png");
	dotSprite = new Sprite;
	dotSprite->setTexture(*dotTexture);
	dotSprite->setScale(0.2f, 0.2f);
	dotSprite->setOrigin(dotTexture->getSize().x / 2, dotTexture->getSize().y / 2);

	gunTexture = new Texture;
	gunTexture->loadFromFile("Sprites/gun.png");
	gunSprite = new Sprite;
	gunSprite->setTexture(*gunTexture);
	gunSprite->setScale(0.4f, 0.4f);
	gunSprite->setOrigin(gunTexture->getSize().x / 2, gunTexture->getSize().y / 2);

	soundBuffer = new SoundBuffer;
	shootEffect = new Sound;
	soundBuffer->loadFromFile("Sounds/shoot.ogg");
	shootEffect->setBuffer(*soundBuffer);

	font = new Font();
	font->loadFromFile("Fonts/tahomabd.ttf");
	livesText = new Text();
	livesText->setFont(*font);
	livesText->setFillColor(Color::Black);
	livesText->setPosition(5, 5);
	livesText->setCharacterSize(14);
	livesText->setString("Vidas: " + std::to_string(lives));
}

void Player::Render(RenderWindow *window)
{
	window->draw(*gunSprite);
	window->draw(*dotSprite);
	window->draw(*livesText);
}

void Player::ResetPlayer()
{
	lives = 3;
}

void Player::PlaySound()
{
	shootEffect->play();
}

void Player::Shoot()
{
	PlaySound();
}

Vector2i Player::GetCurrentPosition()
{
	return mousePosition;
}

void Player::Move(RenderWindow *window)
{
	mousePosition = Mouse::getPosition(*window);

	//Boundaries -screen limits
	if (mousePosition.x < 1) mousePosition.x = 1;
	if (mousePosition.x > 799) mousePosition.x = 799;
	if (mousePosition.y < 1) mousePosition.y = 1;
	if (mousePosition.y > 599) mousePosition.y = 599;

	//dotSprite->setPosition(500, 500);
	dotSprite->setPosition(mousePosition.x, mousePosition.y);
	gunSprite->setPosition(mousePosition.x, 555);
}
