#include "GameOverScreen.h"

GameOverScreen::GameOverScreen(Vector2f startPosition, int score)
{
	position = startPosition;
	finalScore = score;

	font = new Font();
	font->loadFromFile("Fonts/tahomabd.ttf");

	gameOverText = new Text();
	gameOverText->setFont(*font);
	gameOverText->setString("Juego terminado: Puntos: " + std::to_string(score));
	gameOverText->setCharacterSize(22);
	gameOverText->setFillColor(Color::Black);
	gameOverText->setPosition(position);

	questionText = new Text();
	questionText->setFont(*font);
	questionText->setString("Deseas volver a jugar?");
	questionText->setCharacterSize(18);
	questionText->setFillColor(Color::Black);
	questionText->setPosition(position.x + 25, position.y + 30);

	texYes = new Texture;
	texYes->loadFromFile("Sprites/yes button.png");
	sprYes = new Sprite;
	sprYes->setTexture(*texYes);
	sprYes->setScale(0.2f, 0.2f);
	sprYes->setPosition(300, 300);

	texNo = new Texture;
	texNo->loadFromFile("Sprites/no button.png");
	sprNo = new Sprite;
	sprNo->setTexture(*texNo);
	sprNo->setScale(0.2f, 0.2f);
	sprNo->setPosition(400, 300);

}

void GameOverScreen::Render(RenderWindow *w)
{
	if (isShowing)
	{
		w->draw(*gameOverText);
		w->draw(*questionText);
		w->draw(*sprYes);
		w->draw(*sprNo);
	}
}

int GameOverScreen::Response(Vector2f *clickPosition)
{
	Vector2f aux = *clickPosition;
	yesRect = sprYes->getGlobalBounds();
	noRect = sprNo->getGlobalBounds();

	if (yesRect.contains(aux))
	{
		return 1;
	} 
	else if (noRect.contains(*clickPosition))
	{
		return 2;
	}
	else return 0;
	
}

void GameOverScreen::GameOverTime()
{
	isShowing = true;
}

GameOverScreen::~GameOverScreen()
{
	delete font;
	delete gameOverText;
	delete questionText;
	delete sprYes;
	delete sprNo;
	delete texYes;
	delete texNo;
}
